$(document).ready(function() {
    var time = $('#time_limit').val();
    
   if(time) {
       startTime(time);     
   }
});

function startTime(end_time) {
     
     if(end_time.match(/(0-0-0)$/g))
     {
      var id = $('#cube_id').val();

      location.reload();

     }
     else {
      end_time = end_time.split('-');

      end_time[2] = end_time[2] - 1;

      if(end_time[2] < 0) {
          end_time[2] += 60;
          --end_time[1];
          if(end_time[1] < 0) {
              end_time[1] += 60;
              --end_time[0];
          }
      }

      var time =  end_time[0] + '-' + end_time[1] + '-' + end_time[2];
      var time_dif = end_time[0] + ':' + end_time[1];
      document.getElementById('time_print').innerHTML = time;
      if(document.getElementById('time_diff_hidden'))
        document.getElementById('time_diff_hidden').value = time_dif;
      setTimeout(function() {
        startTime(time);
      }, 1000);
    }
}
