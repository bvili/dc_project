jQuery(function(){
    $(document).ready(function(){
        //cancel cube button set cube id to imput
        $(".cancel-cube-button").on('click', function() {
            var id = $(this).attr('data-cube-id');
            $('#anulate_cube_id').val(id);
            $('#refuse_cube_id').val(id);
        });
        $(".delete-cube-button").on('click', function() {
            var id = $(this).attr('data-cube-id');
            $('#delete_cube_id').val(id);
        });
    });
})
