jQuery(function(){
    $(document).ready(function(){
        $("#cube_type").change(function() {
            $('.cub-options').slideUp();
            if($(this).val() == 1 ){
                $('#cub-type-option-challenge').slideDown();
            }
            else if($(this).val() == 0 ){
                $('#cub-type-option-timeout').slideDown();
            }
        });

        $('#cube_date').datetimepicker();
        $('.datepicker-days').on('click', function() {
            $('.datepicker-days').parent().slideUp();
        });

        $(".challenges-number").on('click', function() {
            $(".challenges-container").slideUp();
            var container = $(this).next();
            if(!container.hasClass('active')) {
                container.slideDown();
                $(".challenges-container").removeClass('active');
                container.addClass('active');
            }
            else {
                $(".challenges-container").removeClass('active');
            }
        });
    });
})
