jQuery(function(){
    function Challenge (container, settings){
        this.container = container;
        this.settings = settings;
        this.number = 1;
        this.challenge = container.children().clone();

        this.init();
    }

    Challenge.prototype.init = function() {
        var self = this;

      $(this.settings.addSelector).on('click', function(e) {
            e.stopImmediatePropagation();
            self.setNewChallenge();
        });
    };

    Challenge.prototype.setNewChallenge = function() {
        this.setAttr(this.settings.title);
        this.setAttr(this.settings.description);
        this.setAttr(this.settings.hours);
        this.setAttr(this.settings.min);
        if(this.number === 1) {
            this.showRemoveButton();
        }

        this.number++;

        if(this.number == 6) {
            $(this.settings.addSelector).hide();
        }
        this.challenge.find('#number').text(this.number);
        this.container.append(this.challenge);
        this.challenge = this.challenge.clone();
    };

    Challenge.prototype.setAttr = function(id) {
        input = this.challenge.find('#' + id + '_' + this.number);
        input.attr('id', id + '_' + (this.number+1));
        input.attr('name', id + '_' + (this.number+1));
    };
    Challenge.prototype.showRemoveButton = function() {
        var remove = $(this.settings.removeButtonSelector);
        remove.show();
        var self = this;
        remove.on('click', function(e) {
            e.stopImmediatePropagation();
            console.log(1);
            if(self.number > 1) {
                console.log(self.container.children().last());
                self.container.children().last().remove();
                self.number--;
            }

            if(self.number === 1)
                $(this).hide();
            if(self.number == 5)
                $(self.settings.addSelector).show();
        });
    };

    $(document).ready(function(){
        var settings = {
            'title' : 'challenge_title',
            'description' : 'challenge_description',
            'hours' : 'challenge_hours',
            'min' : 'challenge_min',
            'addSelector' : '.add-challenge-button',
            'buttonListSelector' : '#button-list',
            'removeButtonSelector' : '.remove-challenge-button',
            };
        var container = $('.challenges-container');
        new Challenge(container, settings);
    });
})
