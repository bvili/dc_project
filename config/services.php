<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '836450339841254',
        'client_secret' => 'acd6b186226535eeaab3c4dc8a6ca54b',
        'redirect' => 'http://www.dreamcubic.de/callback',
    ],
    'google' => [
        'client_id' => '445759484036-6h4jnfufj7r7ivk5cr997cnevgspaa4r.apps.googleusercontent.com',
        'client_secret' => 'LtSAg09WuNFE3Tlh3UCMdXaG',
        'redirect' => 'http://www.dreamcubic.de/auth/google/callback',
    ],
];
