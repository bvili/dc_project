<div>
    <h3>Hello,</h3>
        
    <p>
        You have requested a password reset request.
    </p>
    
    <p>If indeed you requested please apply on the link below, otherwise just ignore this email. Thank you!</p>
    <a href="{{$link}}">Look here >>></a>
</div>