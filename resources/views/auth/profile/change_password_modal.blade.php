<div id="changePasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            {!! Form::open(array('url' => 'update-change-password', 'method' => 'POST')) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Current Password</label>
                <div class="col-md-8">
                    <input id="current_password" type="password" class="form-control" name="current_password" value="" required autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">New Password</label>
                <div class="col-md-8">
                    <input id="new_password" type="password" class="form-control" name="new_password" value="" required autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Confirm Password</label>
                <div class="col-md-8">
                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" value="" required autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">
                   <input type="submit" class="btn btn-success" name="Update" value="Update"/>
               </label>
            </div>
            {!! Form::close() !!}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>