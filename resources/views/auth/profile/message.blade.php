<div class="row">
    @if(session('data'))
        @if(isset(session('data')['success']))
        <div class="alert alert-success">
            <strong>Success!</strong> {{session('data')['success']}}
        </div>
        @elseif(isset(session('data')['danger']))
        <div class="alert alert-danger">
            <strong>Error!</strong> {{session('data')['danger']}}
        </div>
        @endif
    @endif
</div>