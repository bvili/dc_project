<div id="userInfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit profile info</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            {!! Form::open(array('url' => 'update-profile-info', 'method' => 'POST')) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                <div class="col-md-8">
                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Name</label>
                <div class="col-md-8">
                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">
                   <input type="submit" class="btn btn-success" name="Update" value="Update"/>
               </label>
            </div>
            {!! Form::close() !!}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>