@extends('layouts.app')

@section('content')
<div class="panel-heading">
    Profile
</div>
<div class="panel-body">
    <div class="row">
        <img src="{{$profile_picture}}" width="200px" height="200px"/>    
    </div>
    <br>
    <div class="row">
        {!! Form::open(array('url' => 'update-profile-image', 'method' => 'POST','files'=>true)) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-1">
                <input type="submit" name="Update" value="Update"/>
            </div>
            <div class="col-md-4">
               {!! Form::file('image_file', array('class' => 'input')) !!}
            </div>
         {!! Form::close() !!}
    </div>
    <br>
    <div class="row">
        <label for="profile_img" class="col-md-1 control-label">Name: </label>
        <div class="col-md-10">
            <span>{{$user->name}}</span>
        </div>
    </div>
    <div class="row">
        <label for="profile_img" class="col-md-1 control-label">Email: </label>
        <div class="col-md-10">
            <span>{{$user->email}}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#userInfoModal">Edit</button>
        </div>
        <div class="col-md-1">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#changePasswordModal">Change Password</button>
        </div>
    </div>
    @include('auth.profile.message')
    @include('auth.profile.edit_user_modal')
    @include('auth.profile.change_password_modal')
</div>
@endsection