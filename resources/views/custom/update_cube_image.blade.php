@extends('layouts.app')

@section('resources_css')
    <link href="/css/contrib/bootstrap/datepicker.css" rel="stylesheet">
    
@endsection

@section('resources_js')
    <script src="/js/cube/addChallenge.js"></script>
@endsection


@section('content')

<div class="panel-heading">
    Update cube Image
</div>
<div class="panel-body">
    
    @include('auth.profile.message')
    
    <form id="form" method="post" action="/update-cube-image-action.php"   enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
    <label>Cube Image:</label> <input type="file" name="cube_gif_view" value="" /> <br>
    <input type="hidden" name="cube_id" value="{{ $cube_id}}"/>
    <input type="submit" class="mybutton" value="Update Image" />
    <br>
</form>
    <br>
    <a href="/cube-list/all">Go to cube list >>  </a>
</div>
@endsection

