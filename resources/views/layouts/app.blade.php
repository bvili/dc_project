<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dream cubic</title>

      <!-- include the BotDetect layout stylesheet -->
   <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">


    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/app_custom.css" rel="stylesheet">
    @yield('resources_css')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
@if(isset($view_cube_timeout))
    <body  onload="startTime()">
@else
    <body>
@endif
    <div class="container my-content" id="app">
        <div class="row app-header">
            <h1>Dream Cubic</h1>
            <br>
        </div>
    <div class="row">
        <nav class="navbar navbar-inverse">

            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand menu-button" href="{{ url('/home') }}">
                    Home
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                    @if (!Auth::guest())
                        <!--<li class="dropdown">

                                <a href="#" class="dropbtn menu-button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Cube
                                    @if(isset($cube_count))
                                        <span class="badge">{{$cube_count}}</span>
                                    @endif
                                    <span class="caret"></span>
                                </a>
                                <div class="dropdown-content">
                                    <a href="{{ url('/create-cube') }}" class="menu-button">Create cube</a>
                                    <a href="{{ url('/cube-list/all') }}"  class="menu-button">Cube list</a>
                                </div>

                        </li>-->
                        <li><a href="{{ url('/create-cube') }}" class="menu-button">Create cube</a></li>
                        <li><a href="{{ url('/cube-list/all') }}"  class="menu-button">Cube list</a></li>
                    @endif

                    @if(Auth::user() && Auth::user()->permission === 1)
                        <li><a href="{{ url('/admin/user-management') }}" class="menu-button">Administrator</a></li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <div>
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}"  class="menu-button">Login</a></li>
                        <li><a href="{{ url('/register') }}"  class="menu-button">Register</a></li>
                    @else
                        <li class="dropdown">

                            <a href="#" class="dropbtn menu-button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php
                                    if(Auth::user()->otherInfo) {
                                        echo '<img width="20px" height="20px" src="'.Auth::user()->otherInfo->profile_image.'"/>';
                                    }
                                ?>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-content">
                                <a href="{{ url('/profile') }}" class="menu-button">Profile</a>
                                <a href="{{ url('/logout') }}" class="menu-button"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>

                        </li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default" id="container_id">
            @yield('content')
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    @yield('resources_js')
</body>
</html>
