@extends('layouts.app')

@section('resources_css')
    <link href="/css/cube/countdown.css" rel="stylesheet">
    <link href="/css/cube/cube_ilustrate.css" rel="stylesheet">

@endsection

@section('resources_js')
     <script src="/js/cube/cube_ilustrate.js"></script>
    <script src="/js/cube/countDown.js"></script>
@endsection


@section('content')

<div id="countdown_container">
    <div class="row">
        <div class="col-md-12" id="title">
            <h1>{{$cube->title}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" id="title">
            <h2>You must wait...</h2>
        </div>
    </div>
    <div class="countdown-container">
        <input id = "end_time" hidden="hidden" type="text" value="{{$time}}">
        <input id = "cube_id" hidden="hidden" type="text" value="{{$cube->id}}">
        <div class="row timer-number">
            <div class="col-md-3 col-xs-3"></div>
            <div class="col-md-1 col-xs-1" id="timer_mounth"></div>
            <div class="col-md-1 col-xs-1" id="timer_day"></div>
            <div class="col-md-1 col-xs-1" id="timer_hour"></div>
            <div class="col-md-1 col-xs-1" id="timer_min"></div>
            <div class="col-md-1 col-xs-1" id="timer_sec"></div>
        </div>
        <div class="row timer-text">
            <div class="col-md-3 col-xs-3"></div>
            <div class="col-md-1 col-xs-1" id="text_mounth"><!--Mounht--></div>
            <div class="col-md-1 col-xs-1" id="text_day">Day</div>
            <div class="col-md-1 col-xs-1" id="text_hour">Hr</div>
            <div class="col-md-1 col-xs-1" id="text_min">Min</div>
            <div class="col-md-1 col-xs-1" id="text_sec">Sec</div>
        </div>
    </div>
    <div id="screen">
        <canvas id="canvas">HTML5 CANVAS</canvas>
        <div id="info">

        </div>
    </div>
</div>
@endsection
