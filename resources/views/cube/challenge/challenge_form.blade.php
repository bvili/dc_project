@extends('layouts.app')

@section('resources_css')
    <link href="/css/challenge/sender_edit.css" rel="stylesheet">
@endsection

@section('resources_js')
        <script src="/js/cube/addChallenge.js"></script>
        <script src="/js/challenge/timeout.js"></script>
@endsection

@section('content')
<div id="edit-form-cointainer">
    <input id="time_limit" hidden="hidden" value="{{$interval}}"/>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$challenge->title}}
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/update_challenge_sender/'. $challenge->cube_id, $challenge->id) }}">
                         {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Time:</label>
                            <div class="col-md-6">
                                <p id="time_print">0-0-0</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Chalenge Description:</label>
                            <div class="col-md-6">
                                <p>&nbsp{{$challenge->description}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Status:</label>
                            <div class="col-md-6">
                                {{ Form::select('status', $status, $challenge->work_status) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Message:</label>
                            <div class="col-md-6">
                                <textarea id="challenge_comment" class="form-control" name="challenge_comment" required autofocus></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">
                                <button type="submit" class="btn btn-success">
                                    Update
                                </button>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3 chat_container">
            @include('cube.challenge.print_chat')
        </div>
    </div>
</div>
@endsection
