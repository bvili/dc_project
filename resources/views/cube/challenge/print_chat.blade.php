<div class="panel panel-default">
  @if($challenge->getChat)
    @foreach($challenge->getChat as $message)
      <div class="row">
        @if($message->who == 0)
          <div class="message_box">
            <span class="you_message left">
              {{$message->message}}
            </span>
          </div>
        @else
        <div class="message_box">
          <span class="he_message right">
            {{$message->message}}
          </span>
        </div>
        @endif
      </div>
    @endforeach
  @endif
</div>
