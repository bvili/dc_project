@extends('layouts.app')

@section('content')

<div class="panel-heading">
    {{$cube->title}}
</div>
<div class="panel-body">
    <table class="table table-hover">
        <theader>
            <tr>
                <th>#Nr</th>
                <th>Tiele</th>
           <!--     <th>Type</th>
                <th>Description</th>-->
                <th>Working Status</th>
           <!--     <th>Accept Status</th>-->
                <th></th>
            </tr>
        </theader>
        <tbody>
        @for($i = 0; $i < count($cube->challenges); $i++)
                <td>{{'#'. ($i+1)}}</td>
                <td>{{$cube->challenges[$i]->title}}</td>
             <!--   <td>{{$cube->challenges[$i]->type}}</td>
                <td>{{$cube->challenges[$i]->description}}</td>-->
                <td>{{$cube->challenges[$i]->getStatus->status}}</td>
              <!--  <td>{{$cube->challenges[$i]->confirm_status}}</td>-->
                <td>
                    <a href="{{ url('/challenge-details/'. $cube->id, ($i+1)) }}">
                        view</span>
                    </a>
                </td>
            </tr>
        @endfor
        </tbody>
    </table>
</div>

@endsection
