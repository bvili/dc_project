<!-- Anulate Modal -->
<div class="modal fade" id="anulate_modal" tabindex="-1" role="dialog" aria-labelledby="anulate_modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="anulate_modalLabel">Anulate cube</h4>
      </div>
      <div class="modal-body">
        Are you sure you want to cancel the cube?
      </div>
      <div class="modal-footer">
        
        {!! Form::open(array('url' => '/cancel-cube', 'method' => 'POST')) !!}
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
            <input type="hidden" name="cube_id" id="anulate_cube_id"/>
            <input type="submit" value="Cancel" class="btn btn-primary"/>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Refuse Modal -->
<div class="modal fade" id="refuse_modal" tabindex="-1" role="dialog" aria-labelledby="refuse_modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="refuse_modalLabel">Anulate cube</h4>
      </div>
      <div class="modal-body">
        Are you sure you want to refuse the cube?
      </div>
      <div class="modal-footer">
        
        {!! Form::open(array('url' => '/cancel-cube', 'method' => 'POST')) !!}
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
            <input type="hidden" name="cube_id" id="refuse_cube_id"/>
            <input type="submit" value="Refuse" class="btn btn-primary"/>
        </form>
      </div>
    </div>
  </div>
</div>    
    
<!-- delete Modal -->
<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="delete_modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="delete_modalLabel">Anulate cube</h4>
      </div>
      <div class="modal-body">
        Are you sure you want to delete the cube?
      </div>
      <div class="modal-footer">
        
        {!! Form::open(array('url' => '/delete-cube', 'method' => 'POST')) !!}
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
            <input type="hidden" name="cube_id" id="delete_cube_id"/>
            <input type="submit" value="Delete" class="btn btn-primary"/>
        </form>
      </div>
    </div>
  </div>
</div>