@extends('layouts.app')

@section('resources_css')
    <link href="/css/contrib/bootstrap/datepicker.css" rel="stylesheet">
    <link href="/css/cube/create_cube.css" rel="stylesheet">
    <link href="/basic/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="/basic/bootstrap-3.3.7/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/basic/bootstrap-3.3.7/css/bootstrap-datetimepicker.css" rel="stylesheet">
@endsection

@section('resources_js')
    <script src="/basic/bootstrap-3.3.7/js/bootstrap-datetimepicker.js"></script>
    <script src="/basic/bootstrap-3.3.7/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/js/cube/create_cube.js"></script>
    <script src="/js/cube/addChallenge.js"></script>
@endsection


@section('content')

<div class="panel-heading">
    Create new Cube
</div>
<div class="panel-body">
    @include('cube.error.message')
    {!! Form::open(array('class' =>"form-horizontal", 'url' => '/create_cube', 'method' => 'POST','files'=>true)) !!}

         {{ csrf_field() }}
        <div class="form-group{{ $errors->has('cube_title') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Cube Title:</label>

            <div class="col-md-6">
                <input id="cube_title" class="form-control" name="cube_title" required autofocus/>
            </div>
        </div>
        <div class="form-group{{ $errors->has('cube_type') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Cube Type:</label>

            <div class="col-md-6">
                <select name="cube_type" id="cube_type">
                    <option value="-1">Select type...</option>
                    <option value="0">Timeout</option>
                    <option value="1">Challenges</option>
                </select>
            </div>
        </div>
        <div id="cub-type-option-timeout" class="cub-options">
            @include('cube.cube_types.timeout_type')
        </div>
        <div id="cub-type-option-challenge" class="cub-options">
            @include('cube.cube_types.challenge_type')
        </div>
        @include('cube.form_end')
    </form>
</div>
@endsection
