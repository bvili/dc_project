<div class="row">

    @if(isset($limit) && !$limit)

        <div class="alert alert-danger">
            <strong>Error!!!! </strong> Free cube created limit is 3 cubes
        </div>

    @endif

    @if(session('data'))
        @if(isset(session('data')['danger']))
            <div class="alert alert-danger">
                {{session('data')['danger']}}
            </div>
        @endif
        
        @if(isset(session('data')['success']))
            <div class="alert alert-success">
                {{session('data')['success']}}
            </div>
        @endif
    @endif
</div>
