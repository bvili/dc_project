@extends('layouts.app')

@section('resources_css')
    <link href="/css/challenge/sender_edit.css" rel="stylesheet">
@endsection

@section('resources_js')
    <script src=""></script>
    <script src="/js/challenge/timeout.js"></script>
@endsection


@section('content')
<div class="cont">
<input id="time_limit" hidden="hidden" value="{{$interval}}"/>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$challenge->title}}
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url($update_link) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Time:</label>
                            <div class="col-md-6">
                                <p id="time_print">0-0-0</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Status:</label>
                            <div class="col-md-6">{{$challenge->getStatus->status}}</div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Description:</label>
                            <div class="col-md-6">{{$challenge->description}}</div>
                        </div>
                        @if($challenge->sender_comment)
                        <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Why recovery:</label>
                                <div class="col-md-6">
                                    <p>{{$challenge->sender_comment}}</p>
                                </div>
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('cube_title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Comment:</label>
                            <div class="col-md-6">
                                <input id="challenge_comment" class="form-control" name="challenge_comment" required autofocus/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Update Finished status:</label>
                            <div class="col-md-6">
                                <input type="hidden" name="time_diff_hidden" id="time_diff_hidden" value="">
                                <input type="checkbox" id="update_status" name="update_status"/>
                            </div>
                        </div>
                        @if($button === 1 || $button === 5)
                            @include('cube.processing_challenges.buttons.finish_challenge');
                        @elseif($button === 2)
                            @include('cube.processing_challenges.buttons.waiting_challenge_result');
                        @elseif($button === 3)
                            @include('cube.processing_challenges.buttons.next_challenge');
                        @elseif($button === 4)
                            @include('cube.processing_challenges.buttons.finishe_all_challenges');
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3 chat_container">
            @include('cube.challenge.print_chat_receiver')
        </div>
    </div>
</div>


@endsection
