
<div class="form-group">
    <div class="col-md-3"></div>
    <div class="col-md-7">
    @if($button === 1)
        <p>If you have met the requirement pressing challenge and expects confirmation of its completion</p>
    @else
        <p>The results were rejected, trying again to continue the challenges.</p>
    @endif
    </div>
</div>
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-success">
            Update Challeng result
        </button>
    </div>
</div>