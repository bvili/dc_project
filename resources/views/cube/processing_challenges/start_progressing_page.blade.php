@extends('layouts.app')

@section('resources_css')
    <link href="" rel="stylesheet">
@endsection

@section('resources_js')
    <script src=""></script>
@endsection


@section('content')
<div class="panel-heading">
    Hello
</div>
<div class="panel-body">
    <div class="row">
        <p> Hello, you're challenged to fulfill these requirements, the success of fulfilling all requirements will reward you with a prize.</p>
    </div>
    <div class="row">
        <p>
            <strong>
                {{$cube->created_user->name}}
            </strong>
            <span>
                has prepared a surprise for you!
            </span>
        </p>
    </div>

    <div class="row">
        <p>
            <strong>
                Message:
            </strong>
            <span>
                "{{$cube->start_message}}"
            </span>
        </p>
    </div>
    <br>
    <center>
        @if($cube->challenges[0]->confirm_status !== 0 || $cube->challenges[0]->work_status === 3)
            <a class="btn btn-lg btn-info" href="/process-challenge/{{$cube->id}}/1">Continue</a>
        @else
            <a class="btn btn-lg btn-success" href="/process-challenge/{{$cube->id}}/1">Start</a>
        @endif
    </center>
</div>
@endsection
