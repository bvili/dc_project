@extends('layouts.app')

@section('resources_css')
    <link href="/css/cube/finished_cube.css" rel="stylesheet">
@endsection

@section('resources_js')
    <script src=""></script>
@endsection


@section('content')
<div class="panel-heading">

</div>
<div class="panel-body">
    <div class="row">
        <center><h1>Congratulation</h1></center>
    </div>
    <hr>

    <div class="row">
        <center><h3>{{$cube->title}}</h3></center>
    </div>
    <hr>

    <div class="row">
        <center><p> {{$context_message}} </p></center>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4 col-sm-2 col-xs-2"></div>
        <div class="col-md-4 col-sm-8 col-xs-8">
            @if($cube->video_source)
                <embed id="video" width="420" height="315" src="http://www.youtube.com/embed/{{$cube->video_source}}?autoplay=1">
            @endif
        </div>
        <div class="col-md-4 col-sm-2 col-xs-2"></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4 col-sm-2 col-xs-2"></div>
        <div class="col-md-4 col-sm-8 col-xs-8">
            @if($cube->image)
                <img id="gif_img" src="{{$cube->image}}" width="420px" height="315px"/>
            @endif
        </div>
        <div class="col-md-4 col-sm-2 col-xs-2"></div>
    </div>
    <hr>
    <div class="row">
        @if($cube->link_source)
        <center>
            <p>
                <strong>
                    Visit link:
                </strong>
                <a href="{{$cube->link_source}}" target="_blank">{{$cube->link_source}}</a>
            </p>
        </center>
        @endif
    </div>
    <hr>
    <div class="row">
        <center>
            <p>
                <strong>
                    Message:
                </strong>
                <span>
                    {{$cube->end_message}}
                </span>
            </p>
        </center>
    </div>
    <br>

</div>
@endsection
