<div class="form-group">
    <label class="col-md-8 control-label">Cube-type Challenge</label>
    <div class="col-md-6"></div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">Decription</label>
    <div class="col-md-6">Cube-type challenge involves choosing the recipient of challenges for the cube (minimum challenging than 6 challenges). Each recipient to meeting challenges you to find content cube.</div>
</div>
<div class="challenges-container">
    <div class="challenges">
        <div class="form-group">
            <label class="col-md-4 control-label"><span id="number">1</span> Challenge title</label>
            <div class="col-md-6">
                <input id="challenge_title_1" class="form-control" name="challenge_title_1"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Decription</label>
            <div class="col-md-6">
                <textarea id="challenge_description_1"  class="form-control" name="challenge_description_1"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Time limit (hours 1-24)</label>
            <div class="col-md-2">
                <input class="time_input" type="number" value="" id="challenge_hours_1" name="challenge_hours_1" min="1" max="24"/> h
            </div>
           
            <div class="col-md-2">
                <input class="time_input" type="number" value="" id="challenge_min_1" name="challenge_min_1" min="1" max="59"/> m
            </div>
        </div>
    </div>
</div>
<div class="form-group" id="button-list">
        <div class="col-md-2"></div>
        <span class="add-challenge-button" href="#"><span class="glyphicon glyphicon-plus"></span></span>
        <span class="remove-challenge-button" href="#"><span class="glyphicon glyphicon-minus"></span></span>
    </div>
<hr>

