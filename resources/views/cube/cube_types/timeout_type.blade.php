<div class="form-group">
    <label class="col-md-8 control-label">Cube-type timeout date</label>
    <div class="col-md-2"></div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">Decription</label>

    <div class="col-md-6">Set the expiration date of the cube for which you want to receive the contents of the cube. (Ex: date of birth)</div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">Set end date</label>
    <div class="col-md-6">
        <input type="text" class="form-control" name="cube_date" value="{{ old('cube_date') }}" id="cube_date"/>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>
