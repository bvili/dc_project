@extends('layouts.app')

@section('resources_css')
    <link href="/css/cube/create_cube.css" rel="stylesheet">
    <link href="/basic/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="/basic/bootstrap-3.3.7/css/bootstrap-theme.min.css" rel="stylesheet">
@endsection

@section('resources_js')
    <script src="/js/cube/list_cube.js"></script>
@endsection


@section('content')

<div class="panel-heading">
    List your all cube
</div>
<div class="panel-body">
    <?php
        echo $message;
    ?>
    @include('cube.error.message')
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#created_cube_list" aria-controls="created_cube_list" role="tab" data-toggle="tab">Created Cubes</a></li>
        <li role="presentation"><a href="#received_cube_list" aria-controls="received_cube_list" role="tab" data-toggle="tab">Received Cubes</a></li>

    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="created_cube_list">

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <!--<th>Type</th>-->
                        <th>Email</th>
                        <!--<th>Message</th>-->
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @for($i = 0; $i < count($cubes); $i++)
                    <tr>
                        <td>{{$cubes[$i]->title}}</td>
                        <!--<td>{{$cubes[$i]->typeNumber->type_name}}</td>-->
                        <td class="email-content">{{$cubes[$i]->email}}</td>
                        <!--<td>{{$cubes[$i]->message}}</td>-->
                        <td>

                            @if($cubes[$i]->email_sending_status == 0)
                                <a class="btn btn-info" href=" {{ url('/mail-send', $cubes[$i]->id) }}">
                                    <span class="glyphicon glyphicon-send"></span>
                                    Send
                                </a>
                            @else
                                <span class="glyphicon glyphicon-send"></span>
                            @endif

                            <!-- View GIF -->
                            <a class="btn btn-success" href="{{ url('/finished-challenge-cube', $cubes[$i]->id) }}">
                                <span class="glyphicon glyphicon-gift"></span>
                                Gif &nbsp; &nbsp;
                            </a>
                             <!-- open cube -->
                            @if($cubes[$i]->type == 0)
                                <a  class="btn btn-info" href="{{ url('/countdown-cube', $cubes[$i]->id) }}">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    Open
                                </a>
                            @else
                                <a class="btn btn-info" href="{{ url('/challenge-cube', $cubes[$i]->id) }}">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    Open
                                </a>
                            @endif
                            <!-- Anulate or delete cube -->
                            @if($cubes[$i]->status == 0)
                                <a href="#" class="btn btn-warning cancel-cube-button" data-toggle="modal" data-target="#anulate_modal" data-cube-id="{{$cubes[$i]->id}}">
                                    <span class="glyphicon glyphicon-off"></span>
                                    Cancel
                                </a>
                            @endif
                            <!--delete cube -->
                            @if($cubes[$i]->status == 5 || $cubes[$i]->status == 6)
                                <a href="#" class="btn btn-danger delete-cube-button"  data-toggle="modal" data-target="#delete_modal"  data-cube-id="{{$cubes[$i]->id}}">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Delete
                                </a>
                            @endif
                        </td>
                    </tr>
                @endfor
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="received_cube_list">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <!--<th>Type</th>-->
                        <th>Sender</th>
                        <!--<th>Sender Email</th>-->
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @for($i = 0; $i < count($cubes_received); $i++)
                    @if($cubes_received[$i]->status == 0)
                    <tr>
                        <td>{{$cubes_received[$i]->title ? $cubes_received[$i]->title : ''}}</td>
                        <!--<td>{{$cubes_received[$i]->typeNumber->type_name}}</td>-->
                        <td>{{$cubes_received[$i]->created_user->name}}</td>
                        <!--<td>{{$cubes_received[$i]->created_user->email}}</td>-->
                        <td>
                        <!-- open cube -->
                            @if($cubes_received[$i]->type == 0)
                                <a class="btn btn-info" href="{{ url('/countdown-cube', $cubes_received[$i]->id) }}">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    Open
                                </a>
                            @else
                                <a class="btn btn-info" href="{{ url('/processing-cube', $cubes_received[$i]->id) }}">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    Open
                                </a>
                            @endif
                        <!-- View GIF -->
                            @if($cubes_received[$i]->gif_view == 1)
                                <a class="btn btn-success" href="{{ url('/finished-challenge-cube', $cubes_received[$i]->id) }}">
                                    <span class="glyphicon glyphicon-gift"></span>
                                    Gif &nbsp; &nbsp;
                                </a>
                            @endif
                        <!-- Refuse -->
                            @if($cubes_received[$i]->status == 0)
                                <a href="#" class="btn btn-danger cancel-cube-button" data-toggle="modal" data-target="#refuse_modal"  data-cube-id="{{$cubes_received[$i]->id}}">
                                    <span class="glyphicon glyphicon-off"></span>
                                    Refuse
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endif
                @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('cube.modal.delete_anulate_modal')

@endsection
