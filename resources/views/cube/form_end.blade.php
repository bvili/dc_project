<div class="form-group{{ $errors->has('cube_start_message') ? ' has-error' : '' }}">
    <label for="cube_start_message" class="col-md-4 control-label">Message Start the cube</label>

    <div class="col-md-6">
        <textarea id="cube_start_message" type="text" class="form-control" name="cube_start_message" value="{{ old('cube_start_message') }}" autofocus></textarea>
        @if ($errors->has('cube_start_message'))
            <span class="help-block">
                <strong>{{ $errors->first('cube_start_message') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr>
<div class="form-group">
    <label for="cube_end_message" class="col-md-4 control-label">Hide things in cube</label>
</div>
<hr>
<div class="form-group{{ $errors->has('cube_end_message') ? ' has-error' : '' }}">
    <label for="cube_end_message" class="col-md-4 control-label">Message</label>
    <div class="col-md-6">
        <textarea id="cube_end_message" type="text" class="form-control" name="cube_end_message" autofocus></textarea>
        @if ($errors->has('cube_end_message'))
            <span class="help-block">
                <strong>{{ $errors->first('cube_end_message') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr>
<div class="form-group{{ $errors->has('cube_video_source') ? ' has-error' : '' }}">
    <label for="cube_video_source" class="col-md-4 control-label">Video</label>
    <div class="col-md-6">
        <input id="cube_video_source" type="url" class="form-control" name="cube_video_source" value="{{ old('cube_video_source') }}" autofocus placeholder="Video source url"/>
        @if ($errors->has('cube_video_source'))
            <span class="help-block">
                <strong>{{ $errors->first('cube_video_source') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr>
<div class="form-group{{ $errors->has('cube_link_source') ? ' has-error' : '' }}">
    <label for="cube_link_source" class="col-md-4 control-label">Link</label>
    <div class="col-md-6">
        <input id="cube_link_source" type="url" class="form-control" name="cube_link_source" value="{{ old('cube_link_source') }}" autofocus placeholder="Website source url"/>
        @if ($errors->has('cube_link_source'))
            <span class="help-block">
                <strong>{{ $errors->first('cube_link_source') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr>
<div class="form-group">
    <label for="cube_image" class="col-md-4 control-label">Check if you want sees gift before completion cube </label>

    <div class="col-md-6">
        <input type="checkbox" name="cube_gif_view" />
    </div>
</div>
<hr>
<div class="form-group{{ $errors->has('cube_email') ? ' has-error' : '' }}">
    <label for="cube_email" class="col-md-4 control-label">E-Mail Address</label>

    <div class="col-md-6">
        <input id="cube_email" type="email" class="form-control" name="cube_email" value="{{ old('cube_email') }}" required autofocus>

        @if ($errors->has('cube_email'))
            <span class="help-block">
                <strong>{{ $errors->first('cube_email') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr>
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        @if($limit)
            <button type="submit" class="btn btn-primary">
        @else
            <button type="submit" class="btn btn-primary" disabled>
        @endif
            Create cube
        </button>
    </div>
</div>
