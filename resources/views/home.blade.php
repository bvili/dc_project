@extends('layouts.app')

@section('content')
<div class="panel-heading">
    Dashboard
</div>
<div class="panel-body">
    Create a dream cube for a friend, and hide in it something nice!
</div>
@endsection
