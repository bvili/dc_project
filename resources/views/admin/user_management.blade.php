@extends('layouts.admin')

@section('resources_css')
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
@endsection

@section('resources_js')
    
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="/js/admin/user_management.js"></script>
@endsection

@section('content')
<div class="container">
    <table id="users-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>#id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created Cube</th>
                <th>Received Cube</th>
                <th>Registrate</th>
            </tr>
        </thead>
        @if($count_all_users > 10)
        <tfoot>
            <tr>
                <th>#id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created Cube</th>
                <th>Received Cube</th>
                <th>Registrate</th>
            </tr>
        </tfoot>
        @endif
        <tbody>
        @for($i = 0; $i < $count_all_users; $i++)
            <tr>
                <td><a href="{{ url('/admin/edit-user', $users[$i]->id) }}">#{{$users[$i]->id}}</a></td>
                <td>{{$users[$i]->name}}</td>
                <td>{{$users[$i]->email}}</td>
                <td>{{count($users[$i]->getCreatedCubes)}}</td>
                <td>{{count($users[$i]->getReceivedCubes)}}</td>
                <td>{{$users[$i]->created_at}}</td>
            </tr>
        @endfor
        </tbody>
    </table>
</div>  
@endsection