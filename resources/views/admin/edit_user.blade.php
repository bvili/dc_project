@extends('layouts.admin')

@section('resources_css')
    <link href="" rel="stylesheet">
@endsection

@section('resources_js')
    <script src=""></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit User
                </div>
                <div class="panel-body">
                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/edit-user-update', $user->id) }}">
                         {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name:</label>

                            <div class="col-md-6">
                                <input id="user_name" class="form-control" name="user_name"  value="{{$user->name ? $user->name : ''}}" required autofocus/>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('user_email') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">E-mail:</label>

                            <div class="col-md-6">
                                <input id="user_email" class="form-control" name="user_email" value="{{$user->email ? $user->email : ''}}" required autofocus/>
                            </div>
                        </div>
			<div class="form-group{{ $errors->has('cube_limit') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Cube Limit:</label>

                            <div class="col-md-6">
                                <input type="number" id="cube_limit" class="form-control" name="cube_limit" value="{{$user->cube_limit ? $user->cube_limit : ''}}" required autofocus/>
                            </div>
                        </div>
			<div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Permission:</label>

                            <div class="col-md-6">
				{{Form::select('permission', $user->all_permissions['select'], $user->all_permissions['default'])}}
                            </div>
                        </div>
                        <!--<div class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Password:</label>

                            <div class="col-md-6">
                                <input id="user_password" class="form-control" name="user_password"  value="{{$user->password ? $user->password : ''}}" required autofocus />
                            </div>
                        </div>-->
                        @if($cond)
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="alert alert-success">
                                        {{$message}}
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>    
        </div>
    </div>
</div>
    
    
@endsection
