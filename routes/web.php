<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

//Route::group(['middleware' => 'web'], function () {
//  Route::auth();
//});

Route::get('/home', 'HomeController@index');
Route::get('/profile', 'Auth\EditUserInfoController@showProfile');


//Route::get('/create-challenge', 'Challenge\CreateChallengeController@printChallengeForm');
/*  Cube pages  */
Route::get('/create-cube', 'Cube\CreateCubeController@createCube');
Route::get('/cube-image-update/{cube_id}', 'Custom\ImageUpdateController@updateImagePage');
Route::get('/cube-list/{email}', 'Cube\ListAllCubeController@printAllCreatedCube');
Route::get('/countdown-cube/{cube_id}', 'Cube\PrintCubeController@printCube');
Route::get('/challenge-cube/{cube_id}', 'Cube\PrintCubeController@printCube');
Route::get('/challenge-details/{cube_id}/{challenge_number}', 'Cube\Challenge\ChallengeController@printChallengeForm');
Route::get('/processing-cube/{cube_id}', 'Cube\ProcessingChallengesController@startProcess');
Route::get('/process-challenge/{cube_id}/{challenge_key}', 'Cube\ProcessingChallengesController@processChallenge');
Route::get('/finished-challenge-cube/{cube_id}', 'Cube\ProcessingChallengesController@printFinishCubePage');
Route::get('/cube-danger-expired-time', 'Cube\ProcessingChallengesController@printFaileCubePage');

//email send
Route::get('/mail-send/{cube_id}', 'Mail\MailSenderController@emailSender');

/* User reset password */
Route::get('/reset-password/{email}', 'Auth\LoginController@resetPassword');


//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('password/email', 'Auth\ResetPasswordController@postEmail');
//login with facebook\

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');
//google login

Route::get('auth/google', 'SocialAuthController@google');
Route::get('auth/google/callback', 'SocialAuthController@googleCallback');

Route::get('/admin/user-management', 'Admin\UserManagementController@listAllUser');
Route::get('/admin/edit-user/{user_id}', 'Admin\UserManagementController@editUser'); //print user form
Route::post('/admin/edit-user-update/{user_id}', 'Admin\UserManagementController@updateUser'); //update user data


//post list
//
Route::post('/create_cube', 'Cube\CreateCubeController@dataProcess');
//Route::post('/create_challenge', 'Challenge\CreateChallengeController@insertChallenge');

//profile info update
Route::post('/update-profile-info', 'Auth\EditUserInfoController@updateProfileInfo');
//profile change password
Route::post('/update-change-password', 'Auth\EditUserInfoController@changePassword');

//update profileimage
Route::post('/update-profile-image', 'Auth\EditUserInfoController@updateImage');
//update cube image
Route::post('/update-cube-image-action.php', 'Custom\ImageUpdateController@updateCubeImagePost');

//update challenge working and confirm data
Route::post('/update-challenge/{cube_id}/{challenge_number}', 'Cube\ProcessingChallengesController@updateChallengeResultData');
Route::post('/update_challenge_sender/{cube_id}/{challenge_id}', 'Cube\ProcessingChallengesController@updateChallengeconformData');

//delete or anulate cube

Route::post('/cancel-cube', 'Cube\CreateCubeController@cancelCube');
Route::post('/delete-cube', 'Cube\CreateCubeController@deleteCube');
