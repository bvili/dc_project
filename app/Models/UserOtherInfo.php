<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOtherInfo extends Model
{
    public $table = 'users_other_info';
}
