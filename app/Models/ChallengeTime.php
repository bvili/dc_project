<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChallengeTime extends Model
{
    protected $table = 'challenge_time';
    public $timestamps = false;
}
