<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    public function getStatus() {
    	return $this->hasOne('App\Models\Status', 'id','work_status');
    }

	public function getTime() {
    	return $this->hasMany('App\Models\ChallengeTime', 'challenge_id','id');
  }

  public function getChat() {
      return $this->hasMany('App\ChallengeChat', 'challenge_id','id');
  }
}
