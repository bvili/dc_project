<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cube extends Model
{
    public function typeNumber() {
    	return $this->hasOne('App\Models\CubeType', 'type_nr','type');
    }
	
	public function challenges() {
    	return $this->hasMany('App\Models\Challenge', 'cube_id','id');
    }
	
	public function created_user() {
    	return $this->hasOne('App\User', 'id','user_id');
    }
	
	public function received_user() {
    	return $this->hasOne('App\User', 'email','email');
    }
}
