<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResetPasswordAlter extends Model
{
    protected $table = "password_resets";
}
