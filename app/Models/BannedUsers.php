<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannedUsers extends Model
{
    protected $table = "banned_users";
}
