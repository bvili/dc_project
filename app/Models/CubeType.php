<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CubeType extends Model
{
    protected $table = 'cube_types';
    
    public static function getCubeTypes() {
        $types = self::get();
        $tp = [];
        foreach($types as $type) {
            $tp[$type->type_nr] = $type->type_name;
        }
        
        return $tp;
    }
}
