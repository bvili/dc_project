<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cube;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    //    $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cubes_received = 0;
        if(Auth::user()) {
            $user_id = Auth::user()->id;
            $user_email = Auth::user()->email;
            $cubes_received = count(Cube::where('email', '=', $user_email)->get());
        }
        
        return view('home', ['cube_count' => $cubes_received]);
    }
}
