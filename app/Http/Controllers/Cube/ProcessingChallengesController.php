<?php

namespace App\Http\Controllers\Cube;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Models\Cube;
use App\Models\Challenge;
use App\Models\ChallengeTime;
use App\ChallengeChat;
use DateTime;

date_default_timezone_set('Europe/Bucharest');

class ProcessingChallengesController extends Controller
{

    public function startProcess(Cube $cube, $cube_id) {

        $cube = $this->getUserCubeByEmail($cube, $cube_id);

        if(!$cube) {
            return view('errors.404');
        }

        return view('cube.processing_challenges.start_progressing_page', ['cube' => $cube]);
    }

    public function processChallenge(Cube $cube, $cube_id, $challenge_key) {

        $cube = $this->getUserCubeByEmail($cube, $cube_id);

        if(!$cube) {
            return view('errors.404');
        }

        if($link = $this->getLastProcessingChallenge($cube, ($challenge_key-1))) {
            return Redirect::to($link);
        }

        $challenges = $cube->challenges;
        if($cube->challenges[$challenge_key-1]->work_status == 4) { //challenge error status
            return Redirect::to('/cube-danger-expired-time');
        }

        $interval = '';
        $time = $cube->challenges[$challenge_key-1]->getTime;

        if( count($time) > 0) {
            $time = $time[count($time)-1]; // get last_time row
        }
        else {
            $time = null;
        }

        if($time && ($cube->challenges[$challenge_key-1]->work_status == 1 || $cube->challenges[$challenge_key-1]->work_status == 3))
        {

            if(!$time->start_time) {
                $challengetime = ChallengeTime::find($time->id);
                $challengetime->start_time = date("Y-m-d H:i:s");
                $challengetime->update();
                $time = $challengetime;
            }
            if(!$interval = $this->getTimeDiff($time->start_time, $time->time_limite)) {
                $challenge = Challenge::find($cube->challenges[$challenge_key-1]->id);

                if($challenge->work_status == 1 && $time->end_time === null) { // if status is started
                    $challenge->work_status = 4; // set error status
                    $challenge->save();
                    return Redirect::to('/cube-danger-expired-time');
                }
            }

            $interval = $interval->h. '-'. $interval->i. '-'. $interval->s;
        }

        $data = $this->getChallengebutton($cube, $challenge_key, $cube_id);
        $update_link = 'update-challenge/'. $cube_id. '/'. $challenge_key;

        return view('cube.processing_challenges.challenge_process_receiver_edit', ['challenge' => $challenges, 'button' => $data[0], 'link' => $data[1], 'challenge' => $cube->challenges[$challenge_key-1], 'update_link' => $update_link, 'interval' => $interval]);
    }

    public function updateChallengeResultData(Cube $cube, Request $request, $cube_id, $challenge_number) {
        $cube = $this->getUserCubeByEmail($cube, $cube_id);

        $challenge_id = $cube->challenges[$challenge_number-1]->id;

        $challenge = Challenge::find($challenge_id);

        $r_message = $request->get('challenge_comment');

        if($r_message) {
            ChallengeChat::insertData($challenge_id, $r_message, 1);
        }

        $challenge->receiver_comment = $request->get('challenge_comment');

        $up_status = $request->get('update_status');

        if($up_status) { // finished challenge
            //update time remaining

            $time_diff_now = $request->get('time_diff_hidden');

            $challenge->work_status = 2;

            if($time_diff_now) {
                $chall_time = ChallengeTime::where('challenge_id', '=', $challenge->id)
                                            ->whereNull('end_time')
                                            ->first();
                $chall_time->end_time = date("Y-m-d H:i:s");
                $chall_time->update();
                //create new time row
                $challengetime = new ChallengeTime();
                $challengetime->challenge_id = $challenge->id;
                $challengetime->time_limite = $time_diff_now;
                $challengetime->save();
            }
        }

        $challenge->update();

        $link = 'process-challenge/'. $cube_id.'/'. $challenge_number;

        return Redirect::back();
    }

    public function updateChallengeconformData(Cube $cube,Request $request, $cube_id, $challenge_id) {

        $challenge = Challenge::find($challenge_id);

        $s_message = $request->get('challenge_comment');

        if($s_message) {
            ChallengeChat::insertData($challenge_id, $s_message, 0);
        }

        if($request) {

            $challenge->work_status = $request->get('status');
            $challenge->confirm_status = 1;

            if($challenge->work_status == 3 || $challenge->work_status == 1) {
                $challenge->confirm_status = 0;
            }

            $challenge->update();

            $link = 'challenge-cube/'. $cube_id. '/'. $challenge_id;
        }


        return Redirect::back();
    }

    public function printFinishCubePage(Cube $cube, $cube_id) {

        $cube = $this->getUserCubeById($cube, $cube_id);

        if(!$cube) {

            $cube = $this->getUserCubeByEmail(new Cube(), $cube_id);

            if(!$cube) {
                return view('errors.404');
            }
        }

        if($cube->video_source) {
           if(preg_match('~.+\=([^\/\&\s]+)$~ims', $cube->video_source, $mat) || preg_match('~.+\/([^\/\&\s]+)$~ims', $cube->video_source, $mat)) {
                $cube->video_source = $mat[1];
           }
           else {
                $cube->video_source = '';
           }
        }

        if($cube->cube_type == 0) {
            $context_message = 'Hello. Your expectations were not in vain, because your patience you have received this gift cube!!!';
        }
        elseif($cube->cube_type == 1) {
            $context_message = "Hello, you're challenged to fulfill these requirements, the success of fulfilling all requirements will reward you with a prize.";
        }


        return view('cube.processing_challenges.finish_cube_page', ['context_message' => $context_message, 'cube' => $cube]);
    }

    public function printFaileCubePage() {
        return view('cube.processing_challenges.faile_cube_page');
    }

    private function getChallengebutton($cube, $challenge_key, $cube_id) {

        if($cube->challenges[$challenge_key-1]->confirm_status == 1) {
            if(isset($cube->challenges[$challenge_key])){
                $link = '/process-challenge/'. $cube_id. '/'. ++$challenge_key;
                $button = 3; //finished and confirm challenge next button for next challenge
            }
            else{
                $button = 4; // finished all challenges
                $link = '/process-challenge/finished-page/'. $cube_id;
            }
        }
        elseif($cube->challenges[$challenge_key-1]->work_status == 3) {
            $button = 5 ;//work challenge, update button
            $link = '/process-challenge/'. $cube_id. '/'. $challenge_key;
        }
        elseif($cube->challenges[$challenge_key-1]->work_status == 2) {
            $link = '#';//finished challenge and waiting for confirm true work
            $button = 2;
        }
        elseif($cube->challenges[$challenge_key-1]->work_status == 1) {
            $button = 1 ;//work challenge, update button
            $link = '/process-challenge/'. $cube_id. '/'. $challenge_key;
        }


        return [$button, $link];
    }

    private function getLastProcessingChallenge(Cube $cube, $challenge_key) {
        $gata = 0;
        $link = null;
        //loop challenge list for get first not finished challenge
        foreach($cube->challenges as $key =>  $challenge) {

            if($challenge->confirm_status == 0) {
                $gata = 1;

                if(($key-1) != $challenge_key && ($key+1) != ($challenge_key+1) ) {

                    $link = 'process-challenge/'. $cube->id. '/'. $key;
                    break;
                }
                else{
                    break;
                }
            }
        }
        if(!$gata) {
            $link = 'finished-challenge-cube/'. $cube->id;
        }
        return $link;
    }

    private function getTimeDiff($time, $time_limit) {

        $datetime2 = new DateTime($time);
        $datetime1 = new DateTime(date('y-m-d H:i:s'));
        $interval = $datetime1->diff($datetime2);

        $time = array();
        list($time['h'], $time['i']) = explode(':', $time_limit);
        if(!$dif = $this->calculDif($interval, $time)) {
            return null;
        }

        return $dif;
    }

    private function calculDif($time_down, $lim) {
        $limit = array();

        $limit['h'] = intval($lim['h']);
        $limit['i'] = intval($lim['i'])-1;
        $limit['s'] = 60;
        if($limit['i'] < 0) {
            $limit['i'] += 60;
            $limit['h']--;
        }

        $time_down->s = $limit['s'] - $time_down->s;
        $time_down->i = $limit['i'] - $time_down->i;
        $time_down->h = $limit['h'] - $time_down->h;
        if($time_down->i < 0) {
            $time_down->h--;
            $time_down->i += 60;
        }

        if($time_down->h < 0)
            return null;

        return $time_down;
    }

}
