<?php

namespace App\Http\Controllers\Cube;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Models\Cube;
use App\Models\CubeType;

class ListAllCubeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function printAllCreatedCube($email) {

        $message = '';

        if(strstr($email, 'emailtrue') !== false) {
            $message = '<div class="alert alert-success">Message has been sent successfully</div>';
        }
        elseif($email == 'emailfalse') {
            $message = '<div class="alert alert-danger">Message has not been sent, please try again later!</div>';
        }
        $cubes = array();

        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $cubes = Cube::where('user_id', '=', $user_id)->get();
        $cubes_received = Cube::where('email', '=', $user_email)->get();

        //$cubes_received = array();
        return view('cube.cube_list', ['cubes' => $cubes, 'cubes_received' => $cubes_received, 'message' => $message] );
    }

}
