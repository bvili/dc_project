<?php

namespace App\Http\Controllers\Cube;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class CreateCubeValidatorController extends Controller
{
    public function validateTimeoutCubeData(Request $request) {
        return  $this->checkRegexValid(['~(\d+\-\d+\-\d+)\s(\d+\:\d+)~ims'], $request->get('cube_date'));
    }

    public function validateGeneralCubeData(Request $request) {

        $video = $request->get('cube_video_source');
        $link = $request->get('cube_link_source');
	
        if($video)
            if(!$this->checkRegexValid(['~https?\:\/\/(?:www)?\.?([\d\WA-z]{5,})~ims'], $video)) {
               return false;
            }
        if($link)
            if(!$this->checkRegexValid(['~https?\:\/\/www\.([\d\WA-z]{5,})~ims'], $link)) {
               return false;
            }
	//die('nooo yess');
        return true;
    }

    private function checkRegexValid(array $regex, $text) {
        foreach($regex as $reg) {
            if(preg_match($reg, $text, $mat)) {
                return true;
            }
        }
        return false;
    }
}
