<?php

namespace App\Http\Controllers\Cube;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cube;
use App\Models\Challenge;
use Datetime;
use Session;

date_default_timezone_set('Europe/Bucharest');


class PrintCubeController extends Controller
{
    public function printCube(Cube $cube, $cube_id) {

        $cube = $this->getUserCubeById($cube, $cube_id);

        if(!$cube) {
            $cube = $this->getUserCubeByEmail(new Cube(), $cube_id);
            
            if(!$cube) {
                return view('errors.404');
            }
        }
        
        //countdpwn cube

        if($cube->type == 0) {
            $datetime2 = new DateTime($cube->timeout);
            $datetime1 = new DateTime(date('y-m-d H:i:s'));
            $interval = $datetime1->diff($datetime2);

            if($datetime2 <= $datetime1) {
                return Redirect::to('/finished-challenge-cube/'.$cube->id);
            }
            else {
                $time = $interval->y. '-'. $interval->m. '-'. $interval->d. '-'. $interval->h. '-'. $interval->i. '-'. $interval->s;
            }

            return view('cube.cube_timeout_print', ['time' => $time, 'view_cube_timeout' => 1, 'mounth' => $interval->m, 'cube' => $cube]);
        }//challenge cube
        elseif($cube->type == 1) {

            return view('cube.cube_challenge_container_print', ['cube' => $cube]);
        }
    }
}
