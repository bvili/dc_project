<?php

namespace App\Http\Controllers\Cube\Challenge;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cube;
use App\Models\Status;
use DateTime;

date_default_timezone_set('Europe/Bucharest');

class ChallengeController extends Controller
{
    public function printChallengeForm(Cube $cube, Request $request, $cube_id, $challenge_number)
    {
        $cube = $this->getUserCubeById($cube, $cube_id);
        
        
        if(!isset($cube->challenges[$challenge_number-1])) { //challenge --number is array key
            return view('errors.404');
        }
        
        $status = new Status();
        
        $status = $status->get()->toArray();
        
        $status = $this->getStatus($status);
        $update_link = '/update-challenge-confirm/'. $cube->id. '/'. $challenge_number;
        $interval = '';
        $time = $cube->challenges[$challenge_number-1]->getTime;
        if(count($time) > 0)
            $time = $time[count($time)-1];
        if(is_object($time))
            if(isset($time->start_time))
            {
                if($interval = $this->getTimeDiff($time->start_time, $time->time_limite)) {
                    $interval = $interval->h. '-'. $interval->i. '-'. $interval->s;
                }
            }
        
        return view('cube.challenge.challenge_form', ['challenge' => $cube->challenges[$challenge_number-1], 'status' => $status, 'update_link' => $update_link, 'interval' => $interval]);  
    }
    
    private function getStatus($status) {
        $s = [];
        foreach($status as $st) {
            $s[$st['id']] = $st['status'];
        }
        
        return $s;
    }
    
    private function getTimeDiff($time, $time_limit) {

        $datetime2 = new DateTime($time);
        $datetime1 = new DateTime(date('y-m-d H:i:s'));
        $interval = $datetime1->diff($datetime2);
        
        $time = array();
        list($time['h'], $time['i']) = explode(':', $time_limit);
        if(!$dif = $this->calculDif($interval, $time)) {
            return null;
        }
        
        return $dif;
    }
    
    private function calculDif($time_down, $lim) {
        $limit = array();
        
        $limit['h'] = intval($lim['h']);
        $limit['i'] = intval($lim['i'])-1;
        $limit['s'] = 60;
        if($limit['i'] < 0) {
            $limit['i'] += 60;
            $limit['h']--;
        }
        
        $time_down->s = $limit['s'] - $time_down->s;
        $time_down->i = $limit['i'] - $time_down->i;
        $time_down->h = $limit['h'] - $time_down->h;
        if($time_down->i < 0) {
            $time_down->h--;
            $time_down->i += 60;
        }
        
        if($time_down->h < 0)
            return null;
        
        return $time_down;
    }
}
