<?php

namespace App\Http\Controllers\Cube;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cube;
use App\Models\Challenge;
use App\Models\ChallengeTime;
use Datetime;
use Session;
use Image;
use Exception;

class CreateCubeController extends Controller
{
    private $validator = null;

    public function createCube() {
        $user = Auth::user();

        $limit = $user->cube_limit ? $user->cube_limit : 3;
        $cubes = Cube::where('user_id', '=', $user->id)->get();

        if(count($cubes) >= $limit) { // limit created cubes
            $limit = false;
        }

        return view('cube.create_cube', ['cubes' => $cubes, 'limit' => $limit]);
    }
    //protected function validator(array $data)
    //{
    //    return Validator::make($data, [
    //        'cube_date' => '',
    //    ]);
    //}

    public function dataProcess(Cube $cube, Request $request) {
        $this->validator = new CreateCubeValidatorController();
        $redirect_imput = false;

        switch($request->get('cube_type'))
        {
            case 0:

                if(!$this->validator->validateTimeoutCubeData($request) || !$this->validator->validateGeneralCubeData($request)) {
                    return back()->with(['data' => ['danger' => 'The data entered is incorrect, try again !!!']]);
                }
                $cube_id = $this->insertTimeoutCube($request);
                break;
            case 1:
                if(!$this->validator->validateGeneralCubeData($request)) {
                    return back()->with(['data' => ['danger' => 'The data entered is incorrect, try again !!!']]);
                }
                $cube_id = $this->insertChallengeCube($request);
                break;
            default:
                return back()->withInput();
        }
        if(!$cube_id)
            return back()->with(['data' => ['danger' => 'Cube has not been saved!']]);

        return Redirect::to('cube-image-update/'. $cube_id)->with('data', ['success' => 'Cube has been saved!']); ;
    }

	public function cancelCube(Request $request, Cube $cube) {
		$id = $request->cube_id;
		if($id) {
			$cube = $cube->find($id);
			$cube->status = 5;
			$cube->update();
		}
		return Redirect::to('cube-list/all')->with(['data' => ['success' => 'Cube was canceled!!']]);
	}

	public function deleteCube(Request $request, Cube $cube) {
		$id = $request->cube_id;
		if($id) {
			$cube = $cube->find($id);
            if(isset($cube->image)) {
                if(!$this->deleteCubeImage($cube->image)) {
                    return Redirect::to('cube-list/all')->with(['data' => ['danger' => 'The image attached to the cube could not be deleted, so the cube was not deleted!!']]);
                }
            }
            $cube->delete();
		}

        return Redirect::to('cube-list/all')->with(['data' => ['danger' => 'Cube was deleted successfully!!']]);
	}

    private function deleteCubeImage($img) {

        if ($img && file_exists(public_path($img)) && !unlink(public_path($img)))
        {
          return false;
        }
        else
        {
          return true;
        }
    }

    private function insertTimeoutCube(Request $request) {

        $insert_data = $request->get('cube_date');

	    $datetime = new Datetime($insert_data);
        $insert_data = $datetime->format('Y-m-d H:i:s');
        $cube = new Cube();

        $cube->title = $request->get('cube_title');
        $cube->type = $request->get('cube_type');
        $cube->user_id = Auth::user()->id;
        $cube->email = $request->get('cube_email');
        $cube->timeout = $datetime;
        $cube->start_message = $request->get('cube_start_message') ? $request->get('cube_start_message') : '';
        $cube->end_message = $request->get('cube_end_message') ? $request->get('cube_end_message') : '';
        $cube->video_source = $request->get('cube_video_source') ? $request->get('cube_video_source') : '';
        $cube->link_source = $request->get('cube_link_source') ? $request->get('cube_link_source') : '';
        $cube->gif_view = $request->get('cube_gif_view') === 'on' ? 1 : 0;

		if($cube->save()) {
			return $cube->id;
		}

		return false;
    }

    private function insertChallengeCube(Request $request) {

        $cube = new Cube();
        $cube->title = $request->get('cube_title');
        $cube->type = $request->get('cube_type');
        $cube->user_id = Auth::user()->id;
        $cube->email = $request->get('cube_email');
        $cube->start_message = $request->get('cube_start_message') ? $request->get('cube_start_message') : '';
        $cube->end_message = $request->get('cube_end_message') ? $request->get('cube_end_message') : '';
        $cube->video_source = $request->get('cube_video_source') ? $request->get('cube_video_source') : '';
        $cube->link_source = $request->get('cube_link_source') ? $request->get('cube_link_source') : '';
        $cube->gif_view = $request->get('cube_gif_view') === 'on' ? 1 : 0;


        if($cube->save()) {

            //save cube image
            for($i = 1; $i <= 6; $i++) {
                $title = $request->get('challenge_title_'. $i);

                if(!!$title) {

                    $challenge_id = $this->insertChallenge([
                                            'title' => $request->get('challenge_title_'. $i),
                                            'description' => $request->get('challenge_description_'. $i),
                                            'cube_id' => $cube->id,
                                            'type' => 1
                                            ]);
                    $this->insertChallengeTimeLimit($request->get('challenge_hours_'. $i), $request->get('challenge_min_'. $i), $challenge_id);
                }
            }
        }

        return $cube->id;
    }

    private function insertChallengeTimeLimit($hours, $min, $challenge_id) {
        $time_limit = '';
        if(!!$hours || !!$min) {

            if(!!$hours) {
                $time_limit = $hours. ':';
            }else {
                $time_limit = '0:';
            }

            if(!!$min) {
                $time_limit .= $min;
            }else {
                $time_limit .= '0';
            }

        }
        if(!!$time_limit) {
            ChallengeTime::insert([
                                               'challenge_id' => $challenge_id,
                                               'time_limite' => $time_limit
                                               ]);
        }
    }

    private function saveImage($request, $cube) {
        $image_path = '/images/cube_gif/';
        $disk_path = public_path($image_path);
        $fileName = 'cube_'. $cube->id. '_gif_img';
        $fileName = hash('ripemd160', $fileName). '.jpg';
        print_r($fileName);die;
        $image = $request->cube_image;
		//print_r($image);die;
        $user = Auth::user();
        if(!$image) {
            return true;
        }
        $image_file = $image->getRealPath();
        if(!strstr($image_file, 'public')) {
			//print_r($image_file);die;
            //print_r($path . $fileName);die;
            if(file_exists($disk_path . $fileName)) { //delete last image
                unlink($disk_path . $fileName);
            }
            $image = Image::make($image_file);
            //$image->resize($this->image_size[0], $this->image_size[1]);
            $image->save($disk_path . $fileName, 100);
        }
        else {
			//print_r($image_file);die;
            return false;
        }

        $cube->image = $image_path. $fileName;
        $cube->save();

        return true;
    }

    private function insertChallenge($challenge) {
        $c = new Challenge();
        $c->title = $challenge['title'];
        $c->type = $challenge['type'];
        $c->description = $challenge['description'];
        $c->cube_id = $challenge['cube_id'];

        $c->save();

        return $c->id;
    }
}
