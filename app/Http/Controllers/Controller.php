<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\Cube;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getUserCubeById(Cube $cube, $cube_id) {
        $user = Auth::user();
        $cube_row = $cube->find($cube_id);

        if(isset($cube_row)) {
            if($cube_row->user_id != $user->id) {
                $cube_row = null;
            }
        }
        else {
            return null;
        }

        return $cube_row;
    }
    
    protected function getUserCubeByEmail(Cube $cube, $cube_id) {
        $user = Auth::user();
        $cube_row = $cube->find($cube_id);

        if(isset($cube_row)) {
            if($cube_row->email != $user->email) {
                $cube_row = null;
            }
        }
        else {
            return null;
        }

        return $cube_row;
    }
}
