<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Image;
use Validator;
use App\Models\UserOtherInfo;
use App\User;
use Crypt;

class EditUserInfoController extends Controller
{
    private $image_size = [320, 240];
    public function showProfile() {
        $user = Auth::user();

        if($user->otherInfo) {
            $picture = $user->otherInfo->profile_image;
        }

        if(!isset($picture)) {
            $picture = 'images/profile/profile_default.jpg';
        }

        return view('auth.profile', ['profile_picture' => $picture, 'user' => $user]);
    }

    public function updateImage(Request $request) {

        $image_path = 'images/profile/';
        $disk_path = public_path($image_path);
        $user = Auth::user();
        $fileName = 'user_'. $user->id. '_profile_picture.jpg';

        if($request->image_file) {

            $image = $request->image_file->getRealPath();

            if($this->saveUserOherInfo($user, $image, $disk_path, $image_path, $fileName)) {
                $message['success'] = 'Profile picture Update Succesfully!';
            }
            else {
                $message['danger'] = 'Profile picture was not updated! Try another image file!!!';
            }

            return redirect()->back()->with('data', $message);

        }
        return back()->withInput();
    }

    private function saveUserOherInfo($user, $image_file, $path, $image_path, $fileName) {

        if(!strstr($image_file, 'public')) {
            //print_r($path . $fileName);die;
            if(file_exists($path . $fileName)) { //delete last image
                unlink($path . $fileName);
            }
            $image = Image::make($image_file);
            $image->resize($this->image_size[0], $this->image_size[1]);
            $image->save($path . $fileName, 100);
        }
        else {
            return false;
        }

        if(!$user->otherInfo) {
            $user_profile = new UserOtherInfo();
            $user_profile->user_id = $user->id;
            $user_profile->profile_image = $image_path. $fileName;
            $user_profile->save();
        }
        else {
            $user_profile = UserOtherInfo::where('user_id', '=', $user->id)->first();
            $user_profile->profile_image = $image_path. $fileName;
            $user_profile->update();
        }

        return true;
    }

    public function updateProfileInfo(Request $request) {
        $user = User::where('email', '=', $request->get('email'))->first();

        $user_info = Auth::user();

        if(!$user || $request->get('email') == $user_info->email) {
            $user = User::find($user_info->id);
            $user->email = $request->get('email');
            $user->name = $request->get('name');
            $user->update();
            $message['success'] = 'Your info is up to date!';
        }
        else {
            $message['danger'] = 'This email is already used!';
        }

        return redirect()->back()->with('data', $message);
    }

    public function changePassword(Request $request) {
        $user_info = Auth::user();

        if (!password_verify($request->get('current_password'), $user_info->password)) {
            $message['danger'] = 'Current password you wrong!';
        }
        elseif(strlen($request->get('new_password')) < 6) {
            $message['danger'] = 'The new password must be at least 6 characters!';
        }
        elseif($request->get('new_password') !== $request->get('confirm_password')) {
            $message['danger'] = 'New password does not match confirmation!';
        }
        else {
            $this->updateUserPassword($user_info->id, $request->get('new_password'));
            $message['success'] = 'The new password has been change!';
        }

        return redirect()->back()->with('data', $message);
    }

    private function updateUserPassword($user_id, $new_password) {
        $user = User::find($user_id);
        $user->password = bcrypt($new_password);
        $user->update();
    }
}
