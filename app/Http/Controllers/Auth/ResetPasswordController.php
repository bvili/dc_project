<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Controllers\Mail\MailSenderController;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\ResetPasswordAlter;
use DateTime;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function postEmail(Request $request)
    {
        
        if(!ResetPasswordAlter::where('email' , '=', $request->get('email'))->first())
        {
            ResetPasswordAlter::insert([
                'email' => $request->get('email'),
                'token' => Password::getRepository()->createNewToken(),
                'created_at' => new DateTime()
                ]);   
        }
        
        $this->validate($request, ['email' => 'required|email']);

        $email = new MailSenderController();
        
        $email->email_title = 'Reset password';
        $email->email_from = $request->get('email');
        
        $email->email_subject = 'Reset password';
        
        $link = 'http://www.dreamcubic.de/reset-password/'. Crypt::encrypt($request->get('email'));
            
        $email->email_body = view('mail.reset_password', ['link' => $link]);
        
        $response = $email->resetPasswordEmail();
        
        return redirect()->back()->with('status', $response);
    }
    
}
