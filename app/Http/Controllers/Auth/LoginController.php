<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\ResetPasswordAlter;
use Illuminate\Support\Facades\Redirect;
use Crypt;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required'
        ]);
    }
    
    public function resetPassword(ResetPasswordAlter $reset_query, $email)
    {
    
        $email = Crypt::decrypt($email);
        
        $reset_query = $reset_query::where('email', '=', $email)->first();
        
        if(!$reset_query) {
            return Redirect::to('login');
        }
        
        $token = $reset_query->token;
        return view('auth.passwords.reset', ['token' => $token]);
    }
}
