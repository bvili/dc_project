<?php

namespace App\Http\Controllers\Mail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cube;
use Illuminate\Support\Facades\Redirect;

class MailSenderController extends Controller
{
     public $email_title = ' has prepared a surprise for you';
     public $email_subject = '';
     public $email_from = null;
     public $email_body = 'test';
     public $cube_link = 'http://www.dreamcubic.de/cube-list/all';
     public function emailSender(Request $request, Cube $cube, $cube_id) {
          $cube = $this->getUserCubeById($cube, $cube_id);
          
          if(!$cube) {
              return view('errors.404');
          }
          
          $this->email_title = $cube->created_user->name. $this->email_title;
          $this->email_from = $cube->email;
          
          $subject = $request->get('title');
          
          if(isset($subject)) {
               $this->email_subject = $subject;
          }
          else{
               $this->email_subject = $cube->title;
          }
          
          $this->email_body = view('mail.content', ['link' => $this->cube_link]);
          
          $message = 'all';
          if($this->email_from) {
               $message = $this->sendEmail($cube);
          }
          
          return Redirect::to('cube-list/'. $message);
     }
     
     public function resetPasswordEmail() {
          return $this->sendEmail();
     }
     
     private function sendEmail($cube = null) {
          
          $mail = new PHPMailer();
          $mail->IsSMTP();
          $mail->SMTPAuth = true; // enable SMTP authentication
          $mail->SMTPSecure = "ssl"; 
          $mail->Host = env('MAIL_HOST', 'user');
          $mail->Port = env('MAIL_PORT', 'user');
          $mail->Username = env('MAIL_USERNAME', 'user');
          $mail->Password = env('MAIL_PASSWORD', 'pass'); 
          $mail->From = env('MAIL_USERNAME', 'user');
          $mail->FromName = $this->email_title;
          $mail->AddAddress($this->email_from);
          $mail->isHTML(true);
          $mail->Subject = $this->email_subject;
          $mail->Body = $this->email_body;
          
          if(!$mail->send()) 
          {
               if($cube) {
                    return 'emailfalse';
               }
               else{
                    return 'Reset password Email sending Failde. Please try again later!!';
               }
               //return  '<div class="alert alert-danger"> Mailer Error: ' . $mail->ErrorInfo. '</div>';
          } 
          else 
          {
               if($cube) {
                    $cube->email_sending_status = 1;
                    $cube->save();
                    return 'emailtrue';
               }
               else {
                    return 'Reset password email sending successfully!!';
               }
               //return '<div class="alert alert-success">Message has been sent successfully</div>';
          }
     }
     
}
