<?php

namespace App\Http\Controllers\Custom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cube;

class ImageUpdateController extends Controller
{
    public function updateImagePage(Cube $cube, int $cube_id) {
        $cube = $this->getUserCubeById($cube, $cube_id);

        if(!$cube) {
            $cube = $this->getUserCubeByEmail(new Cube(), $cube_id);

            if(!$cube) {
                return view('errors.404');
            }
        }

        return view('custom.update_cube_image', ['cube_id' => $cube_id]);
    }

    public function updateCubeImagePost(Request $request) {
        $image = $_FILES['cube_gif_view'];

        if($image['error'] == 4) {
           return back()->withInput()->with('data', ['danger' => 'Please update an image file!!']);
        }

        if($image['error'] != 0) {
           return back()->withInput()->with('data', ['danger' => 'Image update failed!!']);
        }

        if(isset($request->cube_id) && $request->cube_id && $image_file = $this->saveImage($image, $request->cube_id)){
            $cube = new Cube();
            $cube = $cube->find($request->cube_id);
            $cube->image = $image_file;
            $cube->save();
            return back()->withInput()->with('data', ['success' => 'Image update succesfull!!']);
        }

        return back()->withInput()->with('data', ['danger' => serialize($image)]);
    }

    private function saveImage($image, $cube_id) {

        if ((
             (@$image["type"] == "image/gif")
                || (@$image["type"] == "image/jpeg")
                || (@$image["type"] == "image/JPEG")
                || (@$image["type"] == "image/jpg")
                || (@$image["type"] == "image/png")
                || (@$image["type"] == "image/pjpeg")
            )
           )
        {

            $image_path = '/images/cube_gif/';
            $disk_path = public_path($image_path);

            $fileName = 'cube_'. $cube_id. '_gif_img';
            $fileName = hash('ripemd160', $fileName). '.jpg';
            if(@$image["size"] > 600000) {
                $image2 = new SimpleImage();
                $image2->load($image['tmp_name']);
                $image2->resize(400, 400);
                //print_r($image2);die;
                $image2->save($disk_path . $fileName);
                return $image_path . $fileName;
            }

            if (file_exists($image_path . $fileName)){
                unlink($disk_path . $fileName);
            }
            if(@$image["tmp_name"]) {
                move_uploaded_file($image["tmp_name"], $disk_path . $fileName);
            }
            else {
                return false;
            }

            return $image_path . $fileName;
        }

        return false;
    }
}
