<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Socialite;
use App\SocialAccount;
use App\Models\UserOtherInfo;
use App\User;
use Validator;

class SocialAuthController extends Controller
{
    public function __construct()
    {

    }

    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback(SocialAccountService $service)
    {
        $user = Socialite::driver('facebook')->user();
        $avatar = $user->avatar ? $user->avatar : '';
        $user = $service->createOrGetUser($user);

        if($avatar) {
            $this->createUserOtherInfo($user, $avatar);
        }

        auth()->login($user);

        return redirect()->to('/home');
    }

    public function google()
    {
        return Socialite::driver('google')->redirect();
    }


    public function googleCallback()
    {
        try{
            $google = Socialite::driver('google')->user();
        }catch(Exception $e){
            return redirect('/');
        }
        $user = SocialAccount::where('provider_user_id', $google->getId())->first();

        if(!$user){
            $if_user = User::where('email', $google->getEmail())->first();

            if($if_user) {

               //  $validator = Validator::make(['google_acount' => 1],['google_acount' => 2],  ['google_acount' => 'This email este folosit']);
               // ->withErrors($validator)
                return redirect()->to('/home');
            }

            $user = User::create([
                'name' => $google->getName(),
                'email' => $google->getEmail(),
                'password' => bcrypt($google->getId()),
            ]);

            $avatar = $google->getAvatar() ? $google->getAvatar() : '';

            if($avatar) {
                $this->createUserOtherInfo($user, $avatar);
            }


            SocialAccount::create([
                'user_id' => $user->id,
                'provider_user_id' => $google->getId(),
                'provider' => 'google'
            ]);
        }
        else {
            $user = User::where('id', $user->user_id)->first();
        }

        auth()->login($user);
        return redirect()->to('/home');
    }

    private function createUserOtherInfo($user, $avatar) {
        $user_profile = new UserOtherInfo();
        $user_profile->user_id = $user->id;
        $user_profile->profile_image = $avatar;
        $user_profile->save();
    }
}
