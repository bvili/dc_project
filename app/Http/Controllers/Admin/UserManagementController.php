<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Permission;

class UserManagementController extends Controller
{
    public function listAllUser() {
        $users = User::get();
        
        return view('admin.user_management', ['users' => $users, 'count_all_users' => count($users)]);
    }
    
    public function editUser($user_id) {
        $user = User::find($user_id);
        //print_r($user);
        
        $cond = isset($_GET['update']) ? 'true' : false;
        $message = 'User data update successfully!!!';    
        
	if($user) {
	    $user->all_permissions = $this->getAllPermissions($user->permission);
            return view('admin.edit_user', ['user' => $user, 'message'=> $message, 'cond' => $cond]);
	}
    }
    
    public function updateUser(User $user, Request $request, $user_id) {
        $user = $user->find($user_id);
        $user->email = $request->user_email;
        $user->name = $request->user_name;
	$user->cube_limit = $request->cube_limit;
	$user->permission = $request->permission;
        $user->update();
        
        return redirect('admin/edit-user/'. $user_id. '?update');
    }

    public function getAllPermissions($user_permission) {
	$permissions = Permission::get();
	
	$perm = [
		'select' => [],
		'default' => 2,
	];
	foreach($permissions as $p) {
		$perm['select'][$p->permission_nr] = $p->name;
		if($p->permission_nr === $user_permission) //if exist this permission
			 $perm['default'] = $user_permission;
	}
	
	return $perm;	
    }
}
