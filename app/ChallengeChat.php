<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeChat extends Model
{
    protected $table = 'challenge_chat';
    public $timestamps = false;

    public static function insertData($challenge_id, $message, $who) {
      $challenge = new ChallengeChat();

      $challenge->challenge_id = $challenge_id;
      $challenge->message = $message;
      $challenge->who = (intval($who) === 1) ? 1 : 0;

      $challenge->save();
    }
}
