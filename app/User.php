<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function isBanned() {
    	return $this->hasOne('App\Models\BannedUsers', 'user_id','id');
    }
	
	public function getCreatedCubes() {
    	return $this->hasmany('App\Models\Cube', 'user_id','id');
    }
	public function getReceivedCubes() {
    	return $this->hasmany('App\Models\Cube', 'email','email');
    }
	
	public function otherInfo() {
    	return $this->hasOne('App\Models\UserOtherInfo', 'user_id','id');
    }
	public function getPermission() {
        return $this->hasOne('App\Models\Permission', 'permission_nr','permission');
    }

}
