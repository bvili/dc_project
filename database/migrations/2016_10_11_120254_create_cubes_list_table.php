<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCubesListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cubes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->integer('type');
            $table->integer('status')->default(0);
            $table->string('email');
            $table->integer('email_sending_status')->default(0);
            $table->dateTime('timeout')->nullable();
            $table->string('start_message')->default('');
            $table->string('end_message')->default('');
            $table->string('video_source')->default('');
            $table->string('link_source')->default('');
            $table->string('image')->default('');
            $table->integer('gif_view')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
