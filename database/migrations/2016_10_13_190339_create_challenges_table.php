<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cube_id')->unsigned();
            $table->foreign('cube_id')
                  ->references('id')
                  ->on('cubes')
                  ->onDelete('cascade');
            $table->string('title');
            $table->string('description');
            $table->string('type');
            $table->string('geo_locate')->nullable();
            $table->string('sender_comment')->nullable();
            $table->string('receiver_comment')->nullable();
            $table->integer('work_status')->default(1);
            $table->integer('confirm_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chalenges');
    }
}
